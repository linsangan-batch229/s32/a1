// GET - http://localhost:4000/ - send a response Welcome to Booking System
// GET - http://localhost:4000/profile - send a response Welcome to your profile!
// GET - http://localhost:4000/courses - send a response Here’s our courses available
// POST - http://localhost:4000/addcourse - send a response Add a course to our resources
// PUT - http://localhost:4000/updatecourse - send a response Update a course to our resources
// DELETE - http://localhost:4000/archivecourses -send a response Archive courses to our resources

const http = require('http');

const server = http.createServer((request, response) => { 
    let url = request.url;
    let method = request.method;
    let msg = '';
    
    //url - '/' ,  method - 'GET'
    if (url == '/' && method == 'GET'){
        msg = 'Welcome to Booking System';

    //url - '/profile' ,  method - 'GET'
    } else if (url == '/profile' && method == 'GET') {
        msg = 'Welcome to your profile';

    //url - '/courses' ,  method - 'GET'
    } else if (url == '/courses' && method == 'GET') {
        msg = 'Here’s our courses available';

    //url - '/addcourse' ,  method - 'POST'
    } else if (url == '/addcourse' && method == 'POST'){
        msg = 'Add a course to our resources';

    //url - '/updatecourse' ,  method - 'PUT'
    } else if (url == '/updatecourse' && method == 'PUT') {
        msg = 'Update a course to our resources';

    //url - '/archivecourses' ,  method - 'DELETE'
    } else if (url == '/archivecourses' && method == 'DELETE'){
        msg = 'Archive courses to our resources';
    }

 
    response.writeHead(200, {'Content-Type' : 'text/plain'})
    response.end(msg)

}).listen(3000);
console.log('server is running at port 3000');